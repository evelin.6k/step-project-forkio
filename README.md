# Step-project-forkio

#### Состав участников проекта:
- Эвелина Куцин 

#### Какие задачи выполнял кто из участников:
- Задание для студента №1, Задание для студента №2: Эвелина Куцин


### Список использованных технологий
- gulp
- gulp-sass
- browser-sync
- gulp-js-minify
- gulp-uglify
- gulp-clean-css
- gulp-clean
- gulp-concat
- gulp-rename
- gulp-imagemin
- gulp-autoprefixer

### Проект
https://evelin.6k.gitlab.io/step-project-forkio

